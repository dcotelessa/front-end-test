/*BACKBONE*/
var results, indexview, stateview, loginview, workspace;
var gueststatus = false;

var StateInfo = Backbone.Model.extend({
  "name":"",
  "abbreviation":"",
  "capital":"",
  "most-populous-city": "",
  "population": "",
  "square-miles": "",
  "time-zone-1":"",
  "time-zone-2": "",
  "dst":"",

  initialize: function() {
    $("#results").html("");
    new StateView({model: this});
  }

});

var GuestbookInfo = Backbone.Model.extend({
  "user":"",
  "phone":"",
  "message":"",

  initialize: function() {
    new GuestbookView({model: this});
  }

});

/* collections */

var Results = Backbone.Collection.extend({
  url: "/states/abbreviations",

  initialize: function() {
    //menu
    this.fetch({
      success: function(c,r,o){
                  _.each(r, function(e){
                    $('<a></a>').appendTo('#states-selections')
                                .html(e)
                                .wrap('<li></li>')
                                .attr("href","#states/" + e);
                  });
                },
      error: function(){
                  new ErrorView;
                }
    });


    clearMenu();

    if (gueststatus){
      new UserView;
    } else {
      new GuestView;
    }
  },

  renderStates: function(e){
    this.fetch({
      url: "/states/" + e,
      success: function(c,r){
                  c.reset();
                  _.each([r], function(e){
                    c.add(new StateInfo(e));
                  });
                },
      error: function(){
                  new ErrorView;
                }
    });
  },

  renderGuestbook: function(){
    $("#results").html("<h1>Guestbook Entries:</h1>");
    this.fetch({
      url: "/read",
      success: function(c,r){
                  c.reset();
                  _.each(r, function(e){
                    c.add(new GuestbookInfo(e));
                  });
                },
      error: function(){
                  new ErrorView;
                }
    });
  },

  renderGuestform: function(){

    clearMenu();

    if (gueststatus){
      new UserView;
      new GuestbookFormView;
    }else {
      new GuestView;
      new UnAuthorizedView;
    }
  },

  logout: function(){
    this.fetch({
      url: "/logout",
      success: function(){
                  clearMenu();
                  new GuestView;
                  workspace.navigate("", {trigger: true});
                },
      error: function(){
                  new ErrorView;
                }
    });
  }
});

/* Views */
/*would rather use handebars, but for this test, using underscore template is fine */

var IndexView = Backbone.View.extend({
  el: "#results",

  initialize: function(){
    this.render();
  },

  render: function(){
    this.$el.html( '<h1>Welcome to Index Page.</h1>' );
  }

});

var GuestView = Backbone.View.extend({
  el: ".navbar-nav",

  initialize: function(){
    this.render();
  },

  render: function(){
    this.$el.append( '<a id="signin" href="#login" type="button" class="btn btn-default navbar-btn">Login</a>' );
  }

});

var UserView = Backbone.View.extend({
  el: ".navbar-nav",

  initialize: function(){
    this.render();
  },

  events: {
    "click #logout":  "logout"
  },

  render: function(){
    var template = _.template( '<p id="signedin" class="navbar-text">Signed in as <%= user %></p>\n<button id="logout" type="button" class="btn btn-default navbar-btn">Logout</button>' );
    this.$el.append( template({user: gueststatus}) );
  },

  logout: function(){
    results.logout();
  }

});

var StateView = Backbone.View.extend({

  el: "#results",

  initialize: function(){
    this.render();
  },

  render: function(){
    var templates = _.template( "<div id='<%= id %>' class='state-box container'>\n<h1>State Information:</h1>\n</div>");
    this.$el.append( templates({id: this.model.cid}) );
    var attr = this.model.attributes;
    var here = this;
    _.each(attr, function(e,i,o){
      if (e != ""){
        var t = _.template("<p><b><%= i %></b> : <%= e %></p>");
        $("#" + here.model.cid).append(t({i: i, e: e}));
      }
    });
  }

});

var GuestbookView = Backbone.View.extend({

  el: "#results",

  initialize: function(){
    this.render();
  },

  render: function(){
    var id;
    var template = _.template( "<div id='<%= id %>' class='state-box container'>\n</div>");
    this.$el.append(template({id: this.model.cid}));
    var attr = this.model.attributes;
    var here = this;
    _.each(attr, function(e,i,o){
      if (e != ""){
        var t = _.template("<p><b><%= i %></b> : <%= e %></p>");
        $("#" + here.model.cid).append(t({i: i, e: e}));
      }
    });
  }

});

var GuestbookFormView = Backbone.View.extend({

  el: "#results",

  initialize: function(){
    this.render();
  },

  events: {
    "submit .form-write":  "enter"
  },

  render: function(){
    this.$el.html(' <div class="container">\n    <form class="form-write">\n    <h2 class="form-signin-heading">Please write your entry</h2>\n    <label for="phoneInput" class="sr-only">phone</label>\n    <input type="text" name="phone" id="phoneInput" class="form-control" placeholder="Phone" required autofocus>\n    <label for="messageInput" class="sr-only">Message</label>\n    <textarea name="message" id="messageInput" class="form-control" required></textarea>\n <input class="btn btn-lg btn-primary btn-block" type="submit" name="Save"></input>\n    </form>\n</div> <!-- /container -->');
  },

  enter: function(e){
    e.preventDefault();

    var formValues = {
          phone: $('#phoneInput').val(),
          message: $('#messageInput').val()
    };

    $.ajax({
        url: "/write",
        type: 'POST',
        dataType:"json",
        data: formValues,
        success:function (data) {
            $('#login-alert').remove();
            workspace.navigate("/read", {trigger: true});
        },
        error:function(j,s,e) {
          $('#login-alert').remove();
          $('<div id="login-alert" class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error: </span></div>').appendTo('#results .container')
          .append(" " + e);
        }
    });
  }

});

var LoginView = Backbone.View.extend({

  el: "#results",

  initialize: function(){
    this.render();
  },

  events: {
    "submit .form-login":  "login"
  },

  render: function(){
    this.$el.html(' <div class="container">\n    <form class="form-login">\n    <h2 class="form-signin-heading">Please sign in</h2>\n    <label for="loginInput" class="sr-only">username</label>\n    <input type="text" name="user" id="loginInput" class="form-control" placeholder="Username" required autofocus>\n    <label for="passwordInput" class="sr-only">Password</label>\n    <input type="password" name="password" id="passwordInput" class="form-control" placeholder="Password" required>\n <input class="btn btn-lg btn-primary btn-block" type="submit" name="Login"></input>\n    </form>\n</div> <!-- /container -->');
  },

  login: function(e){
    e.preventDefault();

    var formValues = {
          user: $('#loginInput').val(),
          password: $('#passwordInput').val()
    };

    clearMenu();

    $.ajax({
        url: "/login",
        type: 'POST',
        dataType:"json",
        data: formValues,
        success:function (data) {
            $('#login-alert').remove();
            checkLogin();
            clearMenu();
            new UserView;
            workspace.navigate("", {trigger: true});
        },
        error:function(j,s,e) {
          $('#login-alert').remove();
          $('<div id="login-alert" class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error: </span></div>').appendTo('#results .container')
          .append(" " + e);
        }
    });
  }

});

var UnAuthorizedView = Backbone.View.extend({
  el: "#results",

  initialize: function(){
    this.render();
  },

  render: function(){
    this.$el.html( '<h2>You need to login to enter a guestbook entry.</h2>' );
  }

});

var ErrorView = Backbone.View.extend({
  el: "#results",

  initialize: function(){
    this.render();
  },

  render: function(){
    this.$el.html( '<h1>There was an error.</h1>' );
  }

});

/*routes */
var Workspace = Backbone.Router.extend({

  routes: {
    "": "index",  //index
    "login": "login",  //index
    "read": "read",    // #read
    "write": "write",    // #write
    "states/:query": "state",  // #states/state
  },

  index: function(){
    if (indexview){
      indexview.render();
    } else {
      indexview = new IndexView;
    }
  },

  login: function() {
    if (loginview){
      loginview.render();
    } else {
      loginview = new LoginView;
    }
  },

  read: function() {
    results.renderGuestbook();
  },

  write: function() {
    results.renderGuestform();
  },

  state: function(query) {
    results.renderStates(query);
  }

});

/*functions */

function checkLogin(){
  var cookie = $.cookie('login');
  if (cookie){
    gueststatus = cookie;
  } else {
    gueststatus = false;
  }
}

function clearMenu(){

  $("#signedin").remove();
  $("#logout").remove();
  $("#signin").remove();
}

function setGridWidth(){
    $('.dropdown-menu.grid').width($('.navbar').width() - 200);
}
function init(){

  checkLogin();
  //add states

  results = new Results;

  $(window).resize(setGridWidth);

  setGridWidth();

  workspace = new Workspace;
  Backbone.history.start();

}

$(function() {
	init();
});
