frontend-test
==================

## Getting Started

What URL should be used to access your application?

index.html

What libraries did you use to write your application?

Bootstrap, jQuery, jQuery-cookie, Backbone/Underscore.

What influenced the design of your user interface?

*This is a test for simplicity and speed, so I chose Bootstrap which allows for responsive structure and components ready to go. The bootstrap look could then be modified later on, if need be.

What steps did you take to make your application user friendly?

*States are in alphabetical grid upon click to ensure access to each one instead of a list. It works within the Bootstrap to be responsive.

* Dropdown menu from Guest Book selection organizes the viewing and writing of guestbook entries.

* Messages and State Data are set cleanly in seperate boxes. (Bootstrap)

* Menu is fixed to the top of screen to be accessible at all times.

* Bootstrap allows for mobile as well as desktop experiences.


What steps did you take to insure your application was secure?

*Guest cookie is checked upon any view reserved only for users. (Writing Guestbook entries.)

What could be done to the front end or back end to make it more secure?

*On the front end, use a secure http. Also, use a captcha or other turing-like test to ensure a human is logging in.

*On the back end, the password and cookie should be hashed with a salt. On the front end, the password is always hashed with a salt before sending to be checked on the backend.

*On the back end, record the IP addresses of each login : most user login via the same IP's. Also, all calls to information should have a secretID sequence sent that refers to the session created upon login.

ADDENDUM:

There was a missing bit of code in server.js that was causing blank POST calls to the server. I fixed that commented it in server.js
